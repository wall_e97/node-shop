var adminModel = require('../../models/admin/adminModel');
var formidable = require('formidable');

var fs = require('fs');

//Controller dieu huong Product
function getLogin(req, res) {
    res.render('admin/login', {data:{}});
}
function postLogin(req, res) {
    var userMail = req.body.user_mail;
    var userPass = req.body.user_pass;
    var dataLogin = [userMail, userPass];
    var promise = adminModel.login(dataLogin);
    promise.then(function(rs){
        if(rs == 0){
            var error = 'Tài khoản không hợp lệ !';
            res.render('admin/login',{data: {err:error}});
        }
        else{
            res.redirect('/admin/dashboard');
        }
    });
}

function dashboard(req, res) {
     res.render('admin/dashboard');
}
function productList(req, res) {
    var page, per_row, rows_per_page;
	if(req.query.page){
		page = parseInt(req.query.page);
	}
	else{
		page = 1;
	}
	rows_per_page = 5;
	per_row = page*rows_per_page-rows_per_page;

	var promise = adminModel.productList(per_row, rows_per_page);
	promise.then(function(rs){
		var total_rows = adminModel.total('product');
		total_rows.then(function(rs2){
			var number_page = Math.ceil(rs2/rows_per_page);
			var page_prev, page_next;
			page_prev = page - 1;
			if(page_prev == 0){
				page_prev = 1;
			}
			page_next = page + 1;
			if(page_next > number_page){
				page_next = number_page;
			}
			res.render('admin/product', {data:{products:rs, number_page:number_page, page_prev:page_prev, page_next:page_next}});
		});
    });
}

function productGetAdd(req, res) {
    var promise = adminModel.categoryList();
    promise.then(function(rs){
        res.render('admin/add_product', {data:{categories:rs}});
    });
}

function productPostAdd(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files){
        var prd_image = files.prd_image.name;
        var old_path = files.prd_image.path;
        var new_path = './public/images/products/'+prd_image;
        var data = fields;
        delete fields.sbm;
        data.prd_image = prd_image;

        fs.rename(old_path, new_path, function(err){
            if(err){
                throw err;
            }
        })
        adminModel.add('product', data);
        res.redirect('/admin/product/list');
    });
}

function productEdit(req, res) {
    res.render('admin/edit_product');
}

function productDelete(req, res) {
    var del = adminModel.del('product', product_id);
    res.send("product Delete");
}

function categoryList(req, res) {
    var promise = adminModel.list('category');
    promise.then(function(rs){
        res.render('admin/category', {data:{categorys:rs}});
    });
}

function categoryAdd(req, res) {

    res.render('admin/add_category');
}
function categoryEdit(req, res) {
    res.render('admin/edit_category');
}
function categoryDelete(req, res) {
    res.send("Category Delete");
}

function user(req, res){
    res.render('admin/user');
}

function test1(req, res){
    req.session.user_mail = 'vuiahung@gmail.com';
    console.log(req.session.user_mail);
}
function test2(req, res){
    console.log(req.session);
    
}

module.exports = {
    getLogin : getLogin,
    postLogin : postLogin,

    dashboard : dashboard,
    productList : productList,
    productGetAdd : productGetAdd,
    productPostAdd : productPostAdd,

    productEdit : productEdit,
    productDelete : productDelete,

    categoryList : categoryList,
    categoryAdd : categoryAdd,
    categoryEdit : categoryEdit,
    categoryDelete : categoryDelete,

    user : user,

    test1 : test1,
    test2 : test2
}