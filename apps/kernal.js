module.exports = function (express, app, body_parser, session) {
    app.use('/static', express.static(__dirname + '/../public'));  //Cau hinh duong dan tinh (tat)

    app.set('views',__dirname + '/views');  //Cau hinh cho render duong dan tinh (tat)
    app.set('view engine','ejs'); // Cau hinh cho cu file ejs no hieu la engine cua view
    app.use(body_parser.urlencoded({ extended: true })); //Packet dung de truyen va lay chuoi gia tri
    app.use(body_parser.json());

    app.set('trust proxy', 1) // trust first proxy
    app.use(session({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: true,
        cookie: { secure: false } //su dung cho https nen de false
    }));
}