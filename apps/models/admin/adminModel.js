var connection = require('../../../common/database')();
function login(data){
    var promise = new Promise(function(rs, rj){
        connection.query('SELECT * FROM user WHERE user_mail = ? AND user_pass = ?', data ,function (error, results, fields){
            rs(results.length);
        });
    });
    return promise;
}

function productList(per_row, rows_per_page){
    var promise = new Promise(function(rs, rj){
		connection.query(`SELECT * FROM product JOIN category ON product.cat_id=category.cat_id LIMIT ${per_row},${rows_per_page}`, function (error, results, fields) {
            rs(results);
		});
	});
	return promise;
}


function add(table, data){
    connection.query(`INSERT INTO ${table} SET ?`, data, function (error, results, fields) {
        if (error) throw error;
        console.log("OK");
      });
}

function edit(cat){
    connection.query('UPDATE category SET cat_name = ? WHERE cat_id = ?', cat , function (error, results, fields) {
        if (error) throw error;
        console.log("Ok edit")
      });      
}

let edit = (cat) =>{
    connection.query('UPDATE category SET cat_name = ? WHERE cat_id= ?', cat , function (error, results, fields) {
        if (error) throw error;
        let promise = new Promise((rs, rj) =>{
          connection.query('SELECT * FROM product JOIN category ON product.cat_id=category.cat_id LIMIT '+per_row+ ', '+rows_per_page,  
          function (error, results, fields) {         
            rs(results);
          });
        });
        return promise;
      });
      
};

let getProductById = (prd_id) =>{

  let promise = new Promise( (resolve, reject) =>{
    connection.query(  'SELECT * FROM product WHERE prd_id=?', prd_id,  
    function (error, results, fields) {
      resolve(results)
      
    });
  });
  return promise;
}

function del(table, table_id, id){
    connection.query(`DELETE FROM ${table} WHERE ${table_id} = ? `, id , function (error, results, fields) {
        if (error) throw error;
        console.log("Delete oK")
      })
}

function list(table){
    var promise = new Promise(function(rs, rj){
        connection.query(`SELECT * FROM ${table}`, function (error, results, fields){
            rs(results);
        });
    });
    return promise;
} 

function total(table){
    var promise = new Promise(function(rs,rj){
        connection.query(`SELECT * FROM ${table}`, function (error, results, fields){
            rs(results.length);
            console.log(results.length);
        });
    });
    return promise;
}

module.exports = {
    add : add,
    edit : edit,
    del : del,
    list : list,
    login : login,
    productList : productList,
    total : total
}
