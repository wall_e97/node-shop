// var router = require('express').Router();
var adminController = require('../apps/controllers/admin/adminController');
// module.exports = router;

module.exports = function(app){
    app.group('/admin', (router) => {
        router.get('/login', adminController.getLogin);
        router.post('/login', adminController.postLogin);
        router.get('/user', adminController.user);        
        router.get('/dashboard', adminController.dashboard);
        
        app.group('/admin/product', (router) => {
            router.get('/list', adminController.productList);
            router.get('/add', adminController.productGetAdd);
            router.post('/add', adminController.productPostAdd);
            router.get('/edit', adminController.productEdit);
            router.get('/delete', adminController.productDelete);
        });
        
        app.group('/admin/category', (router) => {
            router.get('/list', adminController.categoryList);
            router.get('/add', adminController.categoryAdd);
            router.get('/edit', adminController.categoryEdit);
            router.get('/delete', adminController.categoryDelete);
        });
        
        router.get('/test1', adminController.test1);
        router.get('/test2', adminController.test2);
    });
}