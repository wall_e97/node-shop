var express = require('express');
require('express-group-routes');
var app = express();
var body_parser = require('body-parser');
var session = require('express-session');

require('./apps/kernal')(express, app, body_parser, session);
require('./routes/web')(app);
// app.use('/', require('./routes/web'));

module.exports = app;